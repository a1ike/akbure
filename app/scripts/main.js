$(document).ready(function () {
  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  var helpCounter = 0;
  var groupCounter = 1;

  $('.a-projects__card').wrap('<div class="item" />');

  $('.a-projects__card').each(function (i) {
    if (helpCounter < 2) {
      $(this).addClass('a-projects__small');
      $(this).addClass('a-projects_' + groupCounter);
      helpCounter++;
    } else {
      $(this).addClass('a-projects__big');
      groupCounter++;
      helpCounter = 0;
    }
  });

  for (var i = 1; i <= groupCounter; i++) {
    $('.a-projects_' + i).wrapAll('<div class=\'a-projects__row\' />');
  }

  $('.item').each(function (i) {
    if ($(this).is(':empty')) {
      $(this).remove();
    };
  });

  $('.phone').inputmask('+7(999)999-99-99');

  $('.a-big').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.a-reviews__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.a-projects__cards').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.a-news__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.a-clients__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.a-papers__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.a-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.a-logo__nav').on('click', function (e) {
    $('.a-top').slideToggle('fast', function (e) {});
    $('.a-info').slideToggle('fast', function (e) {});
    $('.a-nav').slideToggle('fast', function (e) {});
  });

  /*  $('.d-service-aside__dropsubul').on('click', function(e) {
    if (
      !$(e.target).closest('.d-service-aside__subul').length &&
      !$(e.target).is('.d-service-aside__subul')
    ) {
      $(this)
        .next()
        .slideToggle('fast', function(e) {
          $(this)
            .prev()
            .parent()
            .toggleClass('d-service-aside__dropsubul_opened');
        });
    }
  });

  $('.d-top__menu').on('click', function(e) {
    $('.d-nav').slideToggle('fast', function(e) {});
  }); */
});
